package models.Diet;

import java.util.TreeSet;
import models.Diet.Water;

public class Diet {
	private int iD;
	private String name;
	private String notes;
	private int days;
	private boolean public_1;
	public TreeSet<Water> water = new TreeSet<Water>();
	public TreeSet<DietDish> dietDish = new TreeSet<DietDish>();
}