package models.Diet;

import models.DishAndProduct.Dish.DishType;
import java.util.TreeSet;
import models.Diet.DietPortion;

public class DietDish {
	private int iD;
	private int position;
	private int day;
	public DishType dishType;
	public TreeSet<DietPortion> dietPortion = new TreeSet<DietPortion>();
}