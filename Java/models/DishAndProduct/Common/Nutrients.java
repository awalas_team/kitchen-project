package models.DishAndProduct.Common;

public class Nutrients {
	private double kcal;
	private double protein;
	private double carbohydrates;
	private double roughage;
	private double sugars;
	private double fat;
	private double cholesterol;
	private double sodium;
	private double potassium;
	private double vitaminA;
	private double vitaminB;
	private double calcium;
	private double iron;
	private double saturated;
	private double polyunsaturated;
	private double monounsaturated;
	private double trans;
	private int iD;
}