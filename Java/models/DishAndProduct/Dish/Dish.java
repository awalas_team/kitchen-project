package models.DishAndProduct.Dish;

import java.util.TreeSet;
import models.DishAndProduct.Common.Photo;
import models.DishAndProduct.Dish.Step;
import models.DishAndProduct.Common.Nutrients;
import models.DishAndProduct.Dish.Component;

public class Dish {
	private int portion;
	private date time;
	private String notes;
	private int iD;
	private String name;
	private String storage;
	private date validity;
	public TreeSet<Photo> photo = new TreeSet<Photo>();
	public TreeSet<Step> step = new TreeSet<Step>();
	public Nutrients nutrients;
	public TreeSet<DishType> dishType = new TreeSet<DishType>();
	public TreeSet<Component> component = new TreeSet<Component>();
}