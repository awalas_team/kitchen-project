package models.DishAndProduct.Product;

import java.util.TreeSet;
import models.DishAndProduct.Common.Photo;
import models.DishAndProduct.Common.Nutrients;

public class Product {
	private int iD;
	private String name;
	private date validity;
	private String storage;
	private String notes;
	public TreeSet<Photo> photo = new TreeSet<Photo>();
	public Nutrients nutrients;
	public Unit unit;

	public int getID() {
		return this.iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setValidity(date validity) {
		this.validity = validity;
	}

	public date getValidity() {
		return this.validity;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getStorage() {
		return this.storage;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNotes() {
		return this.notes;
	}
}