package models.DishAndProduct.Product;

public class Unit {
	private int iD;
	private String name;
	private String description;

	public int getID() {
		return this.iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}
}