package models.User.Dish.Planed;

import java.util.TreeSet;
import models.User.Dish.Planed.PlandedPortion;
import models.DishAndProduct.Dish.DishType;

public class PlanedDishes {
	private int iD;
	private date day;
	private int position;
	private String notes;
	public TreeSet<PlandedPortion> plandedPortion = new TreeSet<PlandedPortion>();
	public DishType dishType;
}