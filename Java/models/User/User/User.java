package models.User.User;

import java.util.TreeSet;
import models.Diet.Diet;
import models.User.Dish.Eated.DrinkWater;
import models.User.Storage.Storage;
import models.User.Dish.Planed.PlanedDiet;
import models.DishAndProduct.Dish.Dish;
import models.User.Dish.Eated.EatedPortion;
import models.User.Shop.Shopping;
import models.User.Dish.Planed.PlanedDishes;

public class User {
	private int iD;
	private String name;
	private String surname;
	private date birthDate;
	private boolean dietetican;
	public User adviser;
	public TreeSet<User> client = new TreeSet<User>();
	public TreeSet<Diet> createdDiet = new TreeSet<Diet>();
	public TreeSet<DrinkWater> drinkWater = new TreeSet<DrinkWater>();
	public TreeSet<UserParam> userParam = new TreeSet<UserParam>();
	public TreeSet<Storage> storage = new TreeSet<Storage>();
	public TreeSet<PlanedDiet> planedDiet = new TreeSet<PlanedDiet>();
	public TreeSet<Dish> dish = new TreeSet<Dish>();
	public TreeSet<EatedPortion> eatedPortion = new TreeSet<EatedPortion>();
	public TreeSet<Shopping> shopping = new TreeSet<Shopping>();
	public TreeSet<PlanedDishes> planedDishes = new TreeSet<PlanedDishes>();
}