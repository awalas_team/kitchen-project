#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.Diet import Water
from models.Diet import DietDish

class Diet(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___name = None
		"""@AttributeType String"""
		self.___notes = None
		"""@AttributeType String"""
		self.___days = None
		"""@AttributeType int"""
		self.___forAll = None
		"""@AttributeType bool"""
		self._water = []
		# @AssociationType models.Diet.Water[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._dietDish = []
		# @AssociationType models.Diet.DietDish[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition

