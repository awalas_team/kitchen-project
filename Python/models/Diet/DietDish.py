#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.DishAndProduct.Dish import DishType
from models.Diet import DietPortion

class DietDish(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___position = None
		"""@AttributeType int"""
		self.___day = None
		"""@AttributeType int"""
		self._dishType = None
		# @AssociationType models.DishAndProduct.Dish.DishType
		# @AssociationMultiplicity 1
		self._dietPortion = []
		# @AssociationType models.Diet.DietPortion[]
		# @AssociationMultiplicity 1..*
		# @AssociationKind Composition

