#!/usr/bin/python
# -*- coding: UTF-8 -*-
class Nutrients(object):
	def __init__(self):
		self.___kcal = None
		"""@AttributeType double"""
		self.___protein = None
		"""@AttributeType double"""
		self.___carbohydrates = None
		"""@AttributeType double"""
		self.___roughage = None
		"""@AttributeType double"""
		self.___sugars = None
		"""@AttributeType double"""
		self.___fat = None
		"""@AttributeType double"""
		self.___cholesterol = None
		"""@AttributeType double"""
		self.___sodium = None
		"""@AttributeType double"""
		self.___potassium = None
		"""@AttributeType double"""
		self.___vitaminA = None
		"""@AttributeType double"""
		self.___vitaminB = None
		"""@AttributeType double"""
		self.___calcium = None
		"""@AttributeType double"""
		self.___iron = None
		"""@AttributeType double"""
		self.___saturated = None
		"""@AttributeType double"""
		self.___polyunsaturated = None
		"""@AttributeType double"""
		self.___monounsaturated = None
		"""@AttributeType double"""
		self.___trans = None
		"""@AttributeType double"""
		self.___iD = None
		"""@AttributeType int"""

