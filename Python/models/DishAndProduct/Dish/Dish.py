#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.DishAndProduct.Common import Photo
from models.DishAndProduct.Dish import Step
from models.DishAndProduct.Common import Nutrients
from models.DishAndProduct.Dish import DishType
from models.DishAndProduct.Dish import Component

class Dish(object):
	def __init__(self):
		self.___portion = None
		"""@AttributeType int"""
		self.___time = None
		"""@AttributeType date"""
		self.___notes = None
		"""@AttributeType String"""
		self.___iD = None
		"""@AttributeType int"""
		self.___name = None
		"""@AttributeType String"""
		self.___storage = None
		"""@AttributeType String"""
		self.___validity = None
		"""@AttributeType date"""
		self._photo = []
		# @AssociationType models.DishAndProduct.Common.Photo[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._step = []
		# @AssociationType models.DishAndProduct.Dish.Step[]
		# @AssociationMultiplicity 1..*
		# @AssociationKind Composition
		self._nutrients = None
		# @AssociationType models.DishAndProduct.Common.Nutrients
		# @AssociationMultiplicity 1
		self._dishType = []
		# @AssociationType models.DishAndProduct.Dish.DishType[]
		# @AssociationMultiplicity 0..*
		self._component = []
		# @AssociationType models.DishAndProduct.Dish.Component[]
		# @AssociationMultiplicity 1..*
		# @AssociationKind Composition

