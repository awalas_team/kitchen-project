#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.DishAndProduct.Common import Photo
from models.DishAndProduct.Common import Nutrients
from models.DishAndProduct.Product import Unit

class Product(object):
	def getID(self):
		"""@ReturnType int"""
		return self.___iD

	def setName(self, aName):
		"""@ParamType aName String"""
		self.___name = aName

	def getName(self):
		"""@ReturnType String"""
		return self.___name

	def setValidity(self, aValidity):
		"""@ParamType aValidity date"""
		self.___validity = aValidity

	def getValidity(self):
		"""@ReturnType date"""
		return self.___validity

	def setStorage(self, aStorage):
		"""@ParamType aStorage String"""
		self.___storage = aStorage

	def getStorage(self):
		"""@ReturnType String"""
		return self.___storage

	def setNotes(self, aNotes):
		"""@ParamType aNotes String"""
		self.___notes = aNotes

	def getNotes(self):
		"""@ReturnType String"""
		return self.___notes

	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___name = None
		"""@AttributeType String"""
		self.___validity = None
		"""@AttributeType date"""
		self.___storage = None
		"""@AttributeType String"""
		self.___notes = None
		"""@AttributeType String"""
		self._photo = []
		# @AssociationType models.DishAndProduct.Common.Photo[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._nutrients = None
		# @AssociationType models.DishAndProduct.Common.Nutrients
		# @AssociationMultiplicity 1
		self._unit = None
		# @AssociationType models.DishAndProduct.Product.Unit
		# @AssociationMultiplicity 1

