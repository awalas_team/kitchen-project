#!/usr/bin/python
# -*- coding: UTF-8 -*-
class Unit(object):
	def getID(self):
		"""@ReturnType int"""
		return self.___iD

	def setName(self, aName):
		"""@ParamType aName String"""
		self.___name = aName

	def getName(self):
		"""@ReturnType String"""
		return self.___name

	def setDescription(self, aDescription):
		"""@ParamType aDescription String"""
		self.___description = aDescription

	def getDescription(self):
		"""@ReturnType String"""
		return self.___description

	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___name = None
		"""@AttributeType String"""
		self.___description = None
		"""@AttributeType String"""

