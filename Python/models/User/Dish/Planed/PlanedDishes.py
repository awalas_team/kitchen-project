#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.User.Dish.Planed import PlandedPortion
from models.DishAndProduct.Dish import DishType

class PlanedDishes(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___day = None
		"""@AttributeType date"""
		self.___position = None
		"""@AttributeType int"""
		self.___notes = None
		"""@AttributeType String"""
		self._plandedPortion = []
		# @AssociationType models.User.Dish.Planed.PlandedPortion[]
		# @AssociationMultiplicity 1..*
		# @AssociationKind Composition
		self._dishType = None
		# @AssociationType models.DishAndProduct.Dish.DishType
		# @AssociationMultiplicity 1

