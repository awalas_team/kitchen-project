#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.User.Shop import Shop
from models.User.Shop import ProductOnList

class Shopping(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___date = None
		"""@AttributeType date"""
		self.___notes = None
		"""@AttributeType String"""
		self.___name = None
		"""@AttributeType String"""
		self._shop = None
		# @AssociationType models.User.Shop.Shop
		# @AssociationMultiplicity 1
		self._productOnList = []
		# @AssociationType models.User.Shop.ProductOnList[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition

