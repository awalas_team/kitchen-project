#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.DishAndProduct.Product import Product

class Supply(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___count = None
		"""@AttributeType double"""
		self.___validity = None
		"""@AttributeType date"""
		self.___notes = None
		"""@AttributeType String"""
		self._product = None
		# @AssociationType models.DishAndProduct.Product.Product
		# @AssociationMultiplicity 1

