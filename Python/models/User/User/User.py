#!/usr/bin/python
# -*- coding: UTF-8 -*-
from models.Diet import Diet
from models.User.Dish.Eated import DrinkWater
from models.User.User import UserParam
from models.User.Storage import Storage
from models.User.Dish.Planed import PlanedDiet
from models.DishAndProduct.Dish import Dish
from models.User.Dish.Eated import EatedPortion
from models.User.Shop import Shopping
from models.User.Dish.Planed import PlanedDishes

class User(object):
	def __init__(self):
		self.___iD = None
		"""@AttributeType int"""
		self.___name = None
		"""@AttributeType String"""
		self.___surname = None
		"""@AttributeType String"""
		self.___birthDate = None
		"""@AttributeType date"""
		self.___dietetican = None
		"""@AttributeType boolean"""
		self._adviser = None
		# @AssociationType models.User.User.User
		# @AssociationMultiplicity 1
		self._client = []
		# @AssociationType models.User.User.User[]
		# @AssociationMultiplicity 0..*
		self._createdDiet = []
		# @AssociationType models.Diet.Diet[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Aggregation
		self._drinkWater = []
		# @AssociationType models.User.Dish.Eated.DrinkWater[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Aggregation
		self._userParam = []
		# @AssociationType models.User.User.UserParam[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Aggregation
		self._storage = []
		# @AssociationType models.User.Storage.Storage[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._planedDiet = []
		# @AssociationType models.User.Dish.Planed.PlanedDiet[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._dish = []
		# @AssociationType models.DishAndProduct.Dish.Dish[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Aggregation
		self._eatedPortion = []
		# @AssociationType models.User.Dish.Eated.EatedPortion[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._shopping = []
		# @AssociationType models.User.Shop.Shopping[]
		# @AssociationMultiplicity 0..*
		# @AssociationKind Composition
		self._planedDishes = []
		# @AssociationType models.User.Dish.Planed.PlanedDishes[]
		# @AssociationMultiplicity 0..*

